#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include "util.h"
#include <string.h>
#include <mpi.h>

/* compuate global residual, assuming ghost values are updated */
double compute_residual(double *lu, int lN, double invhsq)
{
  int i;
  double tmp, gres = 0.0, lres = 0.0;

  for (i = 1; i <= lN*lN; i++){
    int pos = lN+i+2*ceil(1.*i/lN);
    tmp = invhsq*(4.*lu[pos] - (lu[pos+1]+lu[pos-1]+lu[pos+lN+2]+lu[pos-(lN+2)]))-1.;
    lres += tmp * tmp;
  }

  MPI_Allreduce(&lres, &gres, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  return sqrt(gres);
}

int main(int argc, char * argv[])
{
  int mpirank, j, k, i, p, N, lN, iter, max_iters;
  MPI_Status status, status1;
  MPI_Request request_out1, request_in1;
  MPI_Request request_out2, request_in2;
  MPI_Request request_out3, request_in3;
  MPI_Request request_out4, request_in4;

  if (argc != 3) 
   {
      fprintf(stderr, "Function needs number of discretization points in each dimension, and maximum number of iterations as inputs! \n" );
      abort();
   }

   sscanf(argv[1], "%d", &N);
   sscanf(argv[2], "%d", &max_iters);

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);

  /* get name of host running MPI process */
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);
  // printf("Rank %d/%d running on %s.\n", mpirank, p, processor_name);

  /* compute number of unknowns handled by each process */
  j = log(p)/log(2.); 
  j = 0.5*j; // p = 4^j
  
  int divisor = 1; 
  for (i = 0; i < j; i++) {divisor = divisor*2; }
  lN = N/(divisor);
  if ((N % (divisor) != 0) && mpirank == 0 ) {
    // printf("N: %d, local N: %d\n", N, lN);
    // printf("Exiting. N must be a multiple of 2^{%d}\n", j);
    MPI_Abort(MPI_COMM_WORLD, 0);
  }
  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  timestamp_type time1, time2;
  get_timestamp(&time1);

  /* Allocation of vectors, including left/upper and right/lower ghost points */
  double * lu    = (double *) calloc(sizeof(double), (lN+2)*(lN+2));
  double * lunew = (double *) calloc(sizeof(double), (lN+2)*(lN+2));
  double * lutemp;
  double * lleft_bdry = (double *) calloc(sizeof(double),lN); 
  double * lright_bdry = (double *) calloc(sizeof(double),lN); 
  double * lup_bdry = (double *) calloc(sizeof(double),lN); 
  double * ldown_bdry = (double *) calloc(sizeof(double),lN); 
  double * temp_vec = (double *) calloc(sizeof(double),lN); 
  double h = 1.0/(N+1);
  double hsq = h*h;
  double invhsq = 1./hsq;
  double gres, gres0, tol = 1e-5;

  /* initial residual */
  gres0 = compute_residual(lu, lN, invhsq);
  gres = gres0;
  
  // if (mpirank == 0) printf("Initial residual is: %f \n",gres0);
  int pside = sqrt(p);

  for (iter = 0; iter < max_iters && gres/gres0 > tol; iter++) {

    /* Jacobi step for boundary points only */ 

    // First do the first column 
    for (i = 1; i <= lN; i++)
    {
       int pos = lN+i+2;
       lunew[pos] = 0.25*(hsq+lu[pos+1]+lu[pos-1]+lu[pos+lN+2]+lu[pos-(lN+2)]);
    }


    // Then do the last column 
    for (i = 1; i <= lN; i++)
    {
       int pos = lN*(lN+2)+i;
       lunew[pos] = 0.25*(hsq+lu[pos+1]+lu[pos-1]+lu[pos+lN+2]+lu[pos-(lN+2)]);
    }
    
    // Finally do the first and last rows 
    for (i =1; i <= lN-2; i++)
    {
       int pos1 = (lN+2)*(i+1)+1;
       int pos2 = (lN+2)*(i+1)+lN;
       lunew[pos1] = 0.25*(hsq+lu[pos1+1]+lu[pos1-1]+lu[pos1+lN+2]+lu[pos1-(lN+2)]);
       lunew[pos2] = 0.25*(hsq+lu[pos2+1]+lu[pos2-1]+lu[pos2+lN+2]+lu[pos2-(lN+2)]);
    }

    /* Communicate */

    /* communicate ghost values */
    if (mpirank < pside*(pside-1) ) {
      /* If not the last column of processes, send/recv bdry values to the right */
      MPI_Irecv(lright_bdry,lN,MPI_DOUBLE,mpirank+pside,123,MPI_COMM_WORLD,&request_in1);
      for (k = 1; k <= lN; k++) {temp_vec[k-1] = lunew[(lN+2)*lN+k];}
      MPI_Isend((temp_vec),lN,MPI_DOUBLE,mpirank+pside,124,MPI_COMM_WORLD,&request_out1);

    }

    if (mpirank > pside-1 ) {
      /* If not the first column of processes, send/recv bdry values to the left */
      MPI_Irecv(lleft_bdry,lN,MPI_DOUBLE,mpirank-pside,124,MPI_COMM_WORLD,&request_in2);
      for (k = 1; k <= lN; k++) {temp_vec[k-1] = lunew[(lN+2)+k];}
      MPI_Isend((temp_vec),lN,MPI_DOUBLE,mpirank-pside,123,MPI_COMM_WORLD,&request_out2);
    }

   if (mpirank % pside != (pside-1)) {
      /* If not the last row of processes, send/recv bdry values up */
      MPI_Irecv(lup_bdry,lN,MPI_DOUBLE,mpirank+1,125,MPI_COMM_WORLD,&request_in3);
      for (k = 1; k <= lN; k++) {temp_vec[k-1] = lunew[(lN+2)*k+lN];}
      MPI_Isend((temp_vec),lN,MPI_DOUBLE,mpirank+1,126,MPI_COMM_WORLD,&request_out3);
    }

    if (mpirank % pside != 0 ) {
      /* If not the first row of processes, send/recv bdry values down */
      MPI_Irecv(ldown_bdry,lN,MPI_DOUBLE,mpirank-1,126,MPI_COMM_WORLD,&request_in4);
      for (k = 1; k <= lN; k++) {temp_vec[k-1] = lunew[(lN+2)*k+1];}
      MPI_Isend((temp_vec),lN,MPI_DOUBLE,mpirank-1,125,MPI_COMM_WORLD,&request_out4);
    }

    /* Now do some work by iterating over the inner points */ 
    for (i = 1; i <= (lN-2); i++)
    {
      for (k = 1; k <= (lN-2); k++)
      {
         int pos = (lN+2)*(i+1)+1+k;
         lunew[pos] = 0.25*(hsq+lu[pos+1]+lu[pos-1]+lu[pos+lN+2]+lu[pos-(lN+2)]);
      }
    } 

    /* Now check if Isend/Irecv are done and update the old values with the new ones */ 

    if (mpirank < pside*(pside-1) ) {
       MPI_Wait(&request_out1,&status); 
       MPI_Wait(&request_in1,&status); 
    }

    if (mpirank > pside-1 ) {
       MPI_Wait(&request_out2,&status); 
       MPI_Wait(&request_in2,&status); 
    }
   
   if (mpirank % pside != (pside-1)) {
       MPI_Wait(&request_out3,&status); 
       MPI_Wait(&request_in3,&status); 
    }

    if (mpirank % pside != 0 ) {
       MPI_Wait(&request_out4,&status); 
       MPI_Wait(&request_in4,&status); 
    }

    for (i = 1; i <= lN; i++)
    {
       lunew[i] = lleft_bdry[i-1]; 
       lunew[(lN+2)*(lN+1)+i] = lright_bdry[i-1]; 
       lunew[(lN+2)*(i+1)-1] = lup_bdry[i-1];
       lunew[(lN+2)*(i)] = ldown_bdry[i-1];
    }
    
    /* copy newu to u using pointer flipping */
    lutemp = lu; lu = lunew; lunew = lutemp;

    if (0 == (iter % 10)) {
      gres = compute_residual(lu, lN, invhsq);
      if (0 == mpirank) {
	//printf("Iter %d: Residual: %g\n", iter, gres);
      }
    }
  }

  /* Clean up */
  free(lu);
  free(lunew);
  free(lleft_bdry); 
  free(ldown_bdry); 
  free(lright_bdry); 
  free(lup_bdry); 
  free(temp_vec); 

  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  get_timestamp(&time2);
  double elapsed = timestamp_diff_in_seconds(time1,time2);
   if (mpirank == p-1) {
    FILE* fd = NULL;
    char filename[256];
    snprintf(filename, 256, "output_nonblocking%02d.txt", mpirank);
    fd = fopen(filename,"w+");

    if(NULL == fd)
    {
      printf("Error opening file \n");
      return 1;
    }

    if (mpirank == p-1) fprintf(fd,"TIME TAKEN FOR N = %d, p = %d:%f  \n",N,p,elapsed);

    fclose(fd);
  }

  MPI_Finalize();
  return 0;
}
