#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include "util.h"
#include <string.h>
#include <mpi.h>
#include "assert.h"

/* compuate global residual, assuming ghost values are updated */
double compute_residual(double *lu, int lN, double invhsq)
{
  int i;
  double tmp, gres = 0.0, lres = 0.0;

  for (i = 1; i <= lN*lN; i++){
    int pos = lN+i+2*ceil(1.*i/lN);
    tmp = invhsq*(4.*lu[pos] - (lu[pos+1]+lu[pos-1]+lu[pos+lN+2]+lu[pos-(lN+2)]))-1.;
    lres += tmp * tmp;
  }

  MPI_Allreduce(&lres, &gres, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  return sqrt(gres);
}

int main(int argc, char * argv[])
{
  int mpirank, j, k, i, p, N, lN, iter, max_iters;
  MPI_Status status, status1,status2,status3;

  if (argc != 3) 
   {
      fprintf(stderr, "Function needs number of discretization points in each dimension, and maximum number of iterations as inputs! \n" );
      abort();
   }

   sscanf(argv[1], "%d", &N);
   sscanf(argv[2], "%d", &max_iters);

  //printf("pre inits\n");
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpirank);
  MPI_Comm_size(MPI_COMM_WORLD, &p); 
  // printf("post init \n");
  /* get name of host running MPI process */
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);
   printf("Rank %d/%d running on %s.\n", mpirank, p, processor_name);

  /* compute number of unknowns handled by each process */
  
 // printf("pre computing j\n");
  j = log(p)/log(2.); 
  j = 0.5*j; // p = 4^j
  //printf("pre assert \n");
  assert((int)pow(4,j) == p);
 // printf("j is %d \n", j);
  int divis = 1; 
  for (i = 0; i < j; i++) {divis = divis*2;}  

  lN = N/(divis);
  //printf("lN is %d \n", lN);
  if ((N % (divis) != 0) && mpirank == 0 ) {
    //printf("N: %d, local N: %d\n", N, lN);
    //printf("Exiting. N must be a multiple of 2^{%d}\n",j);
    MPI_Abort(MPI_COMM_WORLD, 0);
  }
  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  timestamp_type time1, time2;
  get_timestamp(&time1);

  /* Allocation of vectors, including left/upper and right/lower ghost points */
  double * lu    = (double *) calloc(sizeof(double), (lN+2)*(lN+2));
  double * lunew = (double *) calloc(sizeof(double), (lN+2)*(lN+2));
  double * lutemp;
  double * lleft_bdry = (double *) calloc(sizeof(double),lN); 
  double * lright_bdry = (double *) calloc(sizeof(double),lN); 
  double * lup_bdry = (double *) calloc(sizeof(double),lN); 
  double * ldown_bdry = (double *) calloc(sizeof(double),lN); 
  double * temp_vec = (double *) calloc(sizeof(double),lN); 

  double h = 1.0/(N+1);
  double hsq = h*h;
  double invhsq = 1./hsq;
  double gres, gres0, tol = 1e-5;

  /* initial residual */
  gres0 = compute_residual(lu, lN, invhsq);
  gres = gres0;
  
  int pside = sqrt(p);
  //printf("Pside is %d, p is \n" ,pside);

  // if (0 == mpirank) printf("Initial residual is: %f \n",gres0); 

  for (iter = 0; iter < max_iters && gres/gres0 > tol; iter++) {
    //printf("iter = %d \n",iter);
    /* Iterate only over inner points */ 
    for (i = 1; i <= (lN); i++)
    {
       for (k = 1; k <= (lN); k++) 
       {
         int pos = (lN+2)*(i)+k;
         lunew[pos] = 0.25*(hsq+lu[pos+1]+lu[pos-1]+lu[pos+lN+2]+lu[pos-(lN+2)]);
       }
    } 
    //printf("Iterations over inner points done \n");

    /* communicate ghost values */
    if (mpirank < pside*(pside-1) ) {
      /* If not the last column of processes, send/recv bdry values to the right */
       for (k = 1; k <= lN; k++)
       {
          int pos_send = (lN+2)*lN+k;
          temp_vec[k-1] = lunew[pos_send]; 
       }
      // printf("Starting communication from %d to %d to the right \n",mpirank,mpirank+pside);
       MPI_Send(temp_vec, lN, MPI_DOUBLE, mpirank+pside, 124, MPI_COMM_WORLD);
       MPI_Recv(lright_bdry, lN, MPI_DOUBLE, mpirank+pside, 123, MPI_COMM_WORLD, &status);
      // printf("Communication of last column done from %d to %d to the right\n",mpirank,mpirank+pside);
    }


    if (mpirank > pside-1 ) {
      /* If not the first column of processes, send/recv bdry values to the left */
      for (k = 1; k <= lN; k++)
       {
          int pos_send = (lN+2)+k;
          temp_vec[k-1] = lunew[pos_send]; 
       }
      // printf("Starting communication from %d to %d to the left\n",mpirank,mpirank-pside);
       MPI_Recv(lleft_bdry, lN, MPI_DOUBLE, mpirank-pside, 124, MPI_COMM_WORLD, &status1);
       MPI_Send(temp_vec, lN, MPI_DOUBLE, mpirank-pside, 123, MPI_COMM_WORLD);
      // printf("Communication of first column done from %d to %d to the left\n",mpirank,mpirank-pside);
    }

   if (mpirank % pside != (pside-1)) {
      /* If not the last row of processes, send/recv bdry values up */
      for (k = 1; k <= lN; k++)
      {
         int pos_send = (lN+2)*k+lN;
         temp_vec[k-1] = lunew[pos_send];
      }
     // printf("Starting communication from %d to %d up \n",mpirank,mpirank+1);
      MPI_Send(temp_vec, lN, MPI_DOUBLE, mpirank+1, 125, MPI_COMM_WORLD);
      MPI_Recv(lup_bdry, lN, MPI_DOUBLE, mpirank+1, 126, MPI_COMM_WORLD, &status2);
     // printf("Communication of last row done from %d to %d up \n",mpirank,mpirank+1);
    }

    if (mpirank % pside != 0 ) {
      /* If not the first row of processes, send/recv bdry values down */
      for (k = 1; k <= lN; k++)
      {
         int pos_send = (lN+2)*k+1;
         temp_vec[k-1] = lunew[pos_send];
      }
     // printf("Starting communication from %d to %d down \n",mpirank,mpirank-1);
      MPI_Recv(ldown_bdry, lN, MPI_DOUBLE, mpirank-1, 125, MPI_COMM_WORLD, &status3);
      MPI_Send(temp_vec, lN, MPI_DOUBLE, mpirank-1, 126, MPI_COMM_WORLD);
     // printf("Communication of first row done from %d to %d down \n",mpirank,mpirank-1);
    }


      for (i = 1; i <= lN; i++)
      {
         lunew[i] = lleft_bdry[i-1]; 
         lunew[(lN+2)*(lN+1)+i] = lright_bdry[i-1]; 
         lunew[(lN+2)*(i+1)-1] = lup_bdry[i-1];
         lunew[(lN+2)*(i)] = ldown_bdry[i-1];
      }

     //printf("Updating boundary points complete \n");

    /* Now update the boundary points */ 
    
    /* copy newu to u using pointer flipping */
    lutemp = lu; lu = lunew; lunew = lutemp;

    if (0 == (iter % 10)) {
      gres = compute_residual(lu, lN, invhsq);
      if (0 == mpirank) {
	//printf("Iter %d: Residual: %g\n", iter+1, gres);
      }
    }
  }

  /* Clean up */
  free(lu);
  free(lunew);
  free(lleft_bdry); 
  free(ldown_bdry); 
  free(lright_bdry); 
  free(lup_bdry); 
  free(temp_vec); 

  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  get_timestamp(&time2);
  double elapsed = timestamp_diff_in_seconds(time1,time2);
 
   if (mpirank == p-1) {
    FILE* fd = NULL;
    char filename[256];
    snprintf(filename, 256, "output_blocking%02d.txt", mpirank);
    fd = fopen(filename,"w+");

    if(NULL == fd)
    {
      printf("Error opening file \n");
      return 1;
    }

    if (mpirank == p-1) fprintf(fd,"TIME TAKEN FOR N = %d, p = %d:%f  \n",N,p,elapsed);

    fclose(fd);
  }

  MPI_Finalize();
  return 0;
}
