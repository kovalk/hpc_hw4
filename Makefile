CC = mpicc
FLAGS = -O3 -g
EXECS = jacobi-mpi2D jacobi-nonblocking-mpi2D ssort mpi_solved1 mpi_solved2 mpi_solved3 mpi_solved4  mpi_solved5 mpi_solved6 mpi_solved7

all: ${EXECS}

jacobi-mpi2D: jacobi-mpi2D.c
	${CC} ${FLAGS} $^ -lm -o jacobi-mpi2D

jacobi-nonblocking-mpi2D: jacobi-nonblocking-mpi2D.c
	${CC} ${FLAGS} $^ -lm -o jacobi-nonblocking-mpi2D

mpi_solved1: mpi_solved1.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved1

mpi_solved2: mpi_solved2.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved2

mpi_solved3: mpi_solved3.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved3

mpi_solved4: mpi_solved4.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved4

mpi_solved5: mpi_solved5.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved5

mpi_solved6: mpi_solved6.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved6

mpi_solved7: mpi_solved7.c 
	${CC} ${FLAGS} $^ -lm -o mpi_solved7

ssort: ssort.c
	${CC} ${FLAGS} $^ -lm -o ssort

clean: 
	rm -f ${EXECS}
