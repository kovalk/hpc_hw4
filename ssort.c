/* Parallel sample sort
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <mpi.h>
#include <stdlib.h>
#include "util.h"


static int compare(const void *a, const void *b)
{
  int *da = (int *)a;
  int *db = (int *)b;

  if (*da > *db)
    return 1;
  else if (*da < *db)
    return -1;
  else
    return 0;
}

int main( int argc, char *argv[])
{
  int rank,p;
  int i,j, N;
  int *vec,*sample_vec,*splitters,*split_vec,*lnum_incoming,*index_outgoing,*index_incoming,*lnum_outgoing;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p);

    if (argc != 2) 
   {
      fprintf(stderr, "Function needs N as input!! \n" );
      abort();
   }

   sscanf(argv[1], "%d", &N);

  /* Number of random numbers per processor (this should be increased
   * for actual tests or could be passed in through the command line */


  /* timing */
  MPI_Barrier(MPI_COMM_WORLD);
  timestamp_type time1, time2;
  get_timestamp(&time1);


  vec = calloc(N, sizeof(int));
  splitters = calloc(p-1,sizeof(int)); 

  /* seed random number generator differently on every core */
  srand((unsigned int) (rank + 393919));

  /* fill vector with random integers */
  for (i = 0; i < N; ++i) {
    vec[i] = rand();
  }

  /* sort locally */
  qsort(vec, N, sizeof(int), compare);

  /* randomly sample s entries from vector or select local splitters,
   * i.e., every N/P-th entry of the sorted vector */
  int *lsample_vec; 
  int num_samples = ceil(1.*N/floor(1.*N/p));

  //if (rank == 0) printf("Total number of ints per processor is: %d. Number of samples in each processor is: %d \n", N,num_samples);

  lsample_vec = calloc(num_samples,sizeof(int));
  sample_vec = calloc(num_samples*p, sizeof(int)); 

  //if (rank == 0) printf("Total number of samples is: %d \n",num_samples*p);

  j = 0; 
  for (i = 0; i < N; i+=floor(1.*N/p)) {
    lsample_vec[j] = vec[i];
    // printf(" %d = %d \n", vec[i],lsample_vec[j]);
    j ++; 
  }
  //if (rank == 0) printf("number of samples collected in each processor is: %d \n",j);

  MPI_Gather(&lsample_vec[0],num_samples,MPI_INT,sample_vec,num_samples,MPI_INT,0,MPI_COMM_WORLD);

  /* if (rank == 0){
    for (i = 0; i < num_samples*p; i++) {
      printf("%d  ",sample_vec[i]);
    }
  } */

  if (rank == 0) {
    qsort(sample_vec,num_samples*p,sizeof(int),compare); // First sort the samples
    /* Then choose the p-1 splitters*/
    int index;
    index = floor(1.*(num_samples*p)/(p-1));  
    j = 0; 
    for (i = index-1; i < num_samples*p; i+=index){
        //printf(" %d ", sample_vec[i]);
        splitters[j] = sample_vec[i]; 
        j++; 
    }

    //printf("Number of splitters chosen is: %d \n", j);
  }
  
  MPI_Bcast(splitters,p-1,MPI_INT,0,MPI_COMM_WORLD); // Broadcast the splitters 
  MPI_Barrier(MPI_COMM_WORLD); // Ensure everyone has the splitters

  /* every processor uses the obtained splitters to decide
   * which integers need to be sent to which other processor (local bins) */

  /* Figure out how much memory each processor should allocate memory for */

  lnum_outgoing = calloc(p,sizeof(int)); 
  index_incoming = calloc(p,sizeof(int)); 
  index_outgoing = calloc(p,sizeof(int)); 
  lnum_incoming = calloc(p,sizeof(int)); 


  for (i = 0; i < N; i++){
    for (j = 0; j < p-1; j++){
      if (vec[i] < splitters[j]){
        lnum_outgoing[j] += 1; 
        break; 
      }
      if (vec[i] >= splitters[j] && j == p-2){
        lnum_outgoing[p-1] += 1;
        break;
      }
    }
  }


  MPI_Alltoall(lnum_outgoing,1,MPI_INT,lnum_incoming,1,MPI_INT,MPI_COMM_WORLD);
  int sum = 0; 
  for (i = 0; i < p; i++) { 
    if (i != 0) 
    {  
      index_incoming[i] = index_incoming[i-1]+lnum_incoming[i-1];
      index_outgoing[i] = index_outgoing[i-1]+lnum_outgoing[i-1]; 
    }
    sum += lnum_incoming[i]; 
    //printf("Num incoming to processor %d from processor %d is: %d \n",rank,i,lnum_incoming[i]);
  }
  
  //MPI_Status send_status[p-1], rec_status[p-1];
  //MPI_Request request_out[p-1], request_in[p-1];
  MPI_Status status; 

  /* Allocate enough space for the new array and sort all the values into the appropriate buckets*/ 
  split_vec = calloc(sum, sizeof(int));
  int k = 0;
  int l = 0; 
  for (i = 0; i < p; i++){
    if (i != rank && lnum_outgoing[i] != 0) {
      //printf("Processor %d is trying to send to processor %d %d ints, starting at location %d \n\n",rank,i,lnum_outgoing[i],index_outgoing[i]);
      //MPI_Isend(&vec[index_outgoing[i]],lnum_outgoing[i],MPI_INT,i,rank,MPI_COMM_WORLD,&request_out[k]);
      MPI_Send(&vec[index_outgoing[i]],lnum_outgoing[i],MPI_INT,i,rank,MPI_COMM_WORLD);
      k = k+1; 
    }
    if (i != rank && lnum_incoming[i] != 0) { 
      //printf("Processor %d is trying to recieve from processor %d %d ints, starting at location %d \n\n",rank,i,lnum_incoming[i],index_incoming[i]);
      //MPI_Irecv(&split_vec[index_incoming[i]],lnum_incoming[i],MPI_INT,i,i,MPI_COMM_WORLD,&request_in[l]);
      MPI_Recv(&split_vec[index_incoming[i]],lnum_incoming[i],MPI_INT,i,i,MPI_COMM_WORLD,&status);
      l = l+1; 
    } 
    if (i == rank && lnum_incoming[i] != 0) {
      //printf("Processor %d is trying to send to processor %d %d ints , starting at loation %d \n\n",rank,i,lnum_outgoing[i],index_incoming[i]);
      for (j = 0; j < lnum_outgoing[i]; j++) {
        split_vec[index_incoming[i]+j] = vec[index_outgoing[i]+j];
      }
    }
  }

  qsort(split_vec,sum,sizeof(int),compare);
 
  //MPI_Waitall(p-1,request_out,send_status); 
  //MPI_Waitall(p-1,request_in,rec_status); 

  /* Every processor writes results to a file */

 

  
  
  
/*
  for (i = 0; i < p; i++){
    if (i != rank && lnum_outgoing[i] != 0) {
      printf("Processor %d is trying to send to processor %d %d ints, starting at location %d \n\n",rank,i,lnum_outgoing[i],index_outgoing[i]);
      MPI_Send(&vec[index_outgoing[i]],lnum_outgoing[i],MPI_INT,i,0,MPI_COMM_WORLD);
    }
  } */

  MPI_Barrier(MPI_COMM_WORLD);
  get_timestamp(&time2);
  double elapsed = timestamp_diff_in_seconds(time1,time2);
  {
    FILE* fd = NULL;
    char filename[256];
    snprintf(filename, 256, "output%02d.txt", rank);
    fd = fopen(filename,"w+");

    if(NULL == fd)
    {
      printf("Error opening file \n");
      return 1;
    }

    fprintf(fd, "rank %d's sorted values : [", rank);
    for(i = 0; i < sum; i++)
      fprintf(fd, "  %d ", split_vec[i]); 
    fprintf(fd,"] \n"); 
    if (rank == 0) fprintf(fd,"TIME TAKEN FOR N = %d: %f \n",N,elapsed);

    fclose(fd);
  }

  free(vec);
  free(sample_vec); 
  free(splitters);
  free(split_vec);
  free(lnum_incoming); 
  free(index_outgoing);
  free(index_incoming);
  free(lnum_outgoing); 

  MPI_Finalize();
  return 0;
}
