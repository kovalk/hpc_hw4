/******************************************************************************
* FILE: mpi_bug2.c
* DESCRIPTION: 
*   This program has a bug that causes wrong answers and/or termination - depends
*   upon the MPI library and platform.
* SOURCE: Blaise Barney 
* LAST REVISED: 01/24/09
******************************************************************************/
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
int numtasks, rank, tag, alpha, i;
int beta[10];
MPI_Request reqs_in, reqs_out;
MPI_Status stats_in, stats_out;

MPI_Init(&argc,&argv);
MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
MPI_Comm_rank(MPI_COMM_WORLD, &rank);

if (rank == 0) {
  if (numtasks > 2) 
    printf("Numtasks=%d. Only 2 needed. Ignoring extra...\n",numtasks);
  }

/* Changed tag to be i. Also changed beta to be an array of ints. Print the values proc 1 recieved only
 after all the communication is complete */ 

for (i = 0; i < 10; i++) {
  tag = i; 
  if (rank == 0) { 
    alpha = i*10;
    MPI_Isend(&alpha, 1, MPI_INT, 1, tag, MPI_COMM_WORLD, &reqs_out);
    MPI_Wait(&reqs_out,&stats_out); 
    printf("Proc %d sent %d \n",rank,alpha);
  }
  if (rank == 1) {
    MPI_Irecv(&beta[i], 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &reqs_in);
    MPI_Wait(&reqs_in,&stats_in);
  }
}

if (rank == 1) {
  for (i = 0; i < 10; i++) printf("Proc %d recieved %d \n",rank,beta[i]);
}


MPI_Finalize();
}
